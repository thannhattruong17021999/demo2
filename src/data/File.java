/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.LinkedList;

/**
 *
 * @author USER
 */
public class File {
    public String name;
    int numberOfCharacters;
    LinkedList<Sector> neighbors = new LinkedList<>();

    public File(String name, int numberOfChacracters) {
        this.name = name;
        this.numberOfCharacters = numberOfChacracters;
    }
    public File() {
    }
    
    public void addSectorAddressOnFile(int startPoint, int endPoint){
        Sector neighbor = new Sector(startPoint, endPoint);
        neighbors.add(neighbor);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfCharacters() {
        return numberOfCharacters;
    }

    public void setNumberOfCharacters(int numberOfCharacters) {
        this.numberOfCharacters = numberOfCharacters;
    }
       
    public String listNeighbor(){
        return  neighbors.toString();
    }
    
    public void printSector(){
        System.out.println(name + ": " + listNeighbor());
    }

    public boolean equals(Object obj){
        return this.name == ((File) obj).name;
    }
    
}
