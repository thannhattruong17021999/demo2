/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.LinkedList;
import util.MyToys;

/**
 *
 * @author USER
 */
public class Disk {

    char[] disk = new char[300];
    LinkedList<File> filesList = new LinkedList<>();
    LinkedList<Sector> pool = new LinkedList<>();

    public void valueOriginalOfPool() {
        pool.add(new Sector(0, 100));
    }

    public void addSector(File fileName) {
        filesList.add(fileName);
    }

    public void addFile(String name, int size) {
        File v = new File(name, size);
        filesList.add(v);
    }

    public File getSector(String name) {
        for (int i = 0; i < filesList.size(); i++) {
            if (filesList.get(i).name.equals(name)) {
                return filesList.get(i);
            }
        }
        return null;
    }

    public void createFile() {
        String name;
        String content;
        int numberOfCharacter;

        do {
            name = MyToys.getString("Input name file: ", "Input error");
            if (checkFileExist(name) != -1) {
                System.out.println("File name is exist");
            }
        } while (checkFileExist(name) != -1);
        content = MyToys.getString("Input content of file: ", "Input error");
        numberOfCharacter = content.length();
        File newFile = new File(name, numberOfCharacter);

        int numOfSecForNewFile = (int) Math.ceil(((double) numberOfCharacter) / 3);
        int lastSector = numberOfCharacter % 3;
        if (lastSector == 0) {
            lastSector = 3;
        }

        int numOfSecSaved = 0;
        int pos = 0;
        int start;
        int end;
        int i;
        for (i = 0; i < pool.size(); i++) {
            int start1 = 0;
            start = pool.get(i).getStartPoint();
            end = pool.get(i).getEndPoint();
            for (int j = start; j <= end && numOfSecSaved < numOfSecForNewFile; j++) {
                if (((numOfSecSaved + 1) == numOfSecForNewFile) && lastSector != 3) {
                    for (int k = 3 * j; k < (3 * (j + 1) - (3 - lastSector)); k++) {
                        disk[k] = content.charAt(pos);
                        pos++;
                    }
                    numOfSecSaved++;

                } else {
                    for (int k = 3 * j; k < 3 * (j + 1) && numOfSecSaved < numOfSecForNewFile; k++) {
                        disk[k] = content.charAt(pos);
                        pos++;
                    }
                    numOfSecSaved++;

                }
                start1 = j;
                removeAFirstSectorFromPool();
            }
            newFile.addSectorAddressOnFile(start, start1);           

            if (numOfSecSaved == numOfSecForNewFile) {
                break;
            }
            i--;

        }
        filesList.add(newFile);
    }

    public void deleteFile() {
        String name;
        if (filesList.isEmpty()) {
            System.out.println("Disk have no file");
        }
        name = MyToys.getString("Input a file name: ", "Input error");
        int position = checkFileExist(name);
        if (position != -1) {
            for (int j = 0; j < filesList.get(position).neighbors.size(); j++) {
                addANewSectorToPool(filesList.get(position).neighbors.get(j));
            }
            filesList.remove(position);
            System.out.println("Delete file sucessful");
        } else {
            System.out.println("Not found");
        }

    }

    public void addANewSectorToPool(Sector sector) {
        for (int i = 0; i < pool.size(); i++) {
            if ((sector.getEndPoint() + 1) < pool.get(i).getStartPoint()) {
                pool.add(i, sector);
                return;
            } else if (pool.get(i).getEndPoint() == (sector.getStartPoint() - 1) && (sector.getEndPoint() + 1) == pool.get(i + 1).getStartPoint()) {
                pool.get(i).setEndPoint(pool.get(i + 1).getEndPoint());
                pool.remove(i + 1);
                return;
            } else if ((sector.getEndPoint() + 1) == pool.get(i).getStartPoint()) {
                pool.get(i).setStartPoint(sector.getStartPoint());
                return;
            } else if ((sector.getStartPoint() - 1) == pool.get(i).getEndPoint()) {
                pool.get(i).setEndPoint(sector.getEndPoint());
                return;
            }
        }
    }

    public int checkFileExist(String nameFile) {
        if (filesList.isEmpty()) {
            return -1;
        }
        for (int i = 0; i < filesList.size(); i++) {
            if (filesList.get(i).name.equals(nameFile)) {
                return i;
            }
        }
        return -1;
    }

    public void showDisk() {
        System.out.println("Disk: ");
        for (int i = 0; i < 100; i++) {
            if (i % 10 == 0) {
                System.out.println("");
            }
            System.out.printf("| ");
            for (int j = 3 * i; j < 3 * (i + 1); j++) {
                System.out.printf("%c", disk[j]);
            }
            System.out.printf(" |");
        }

    }

    public void showSectorOfAFile() {
        String name = MyToys.getString("Input file: ", "Input error");
        int positon = getPositionFile(name);
        if (positon == -1) {
            System.out.println("File is not exist");
        } else {
            System.out.printf("File name %s: \n", name);
            int count = 0;
            for (int i = 0; i < filesList.get(positon).neighbors.size(); i++) {
                System.out.println("Node " + i + ":");
                System.out.println("    Start sector: " + filesList.get(positon).neighbors.get(i).getStartPoint());
                System.out.println("    End sector: " + filesList.get(positon).neighbors.get(i).getEndPoint());
            }

            for (int i = 0; i < filesList.get(positon).neighbors.size(); i++) {
                int startPoinOfANodeOfSector = filesList.get(positon).neighbors.get(i).getStartPoint();
                int endPoinOfANodeOfSector = filesList.get(positon).neighbors.get(i).getEndPoint();
                int numberOfCharaters = filesList.get(positon).getNumberOfCharacters();
                for (int j = 3 * startPoinOfANodeOfSector; j < 3 * (endPoinOfANodeOfSector + 1) && count < numberOfCharaters; j++) {
                    System.out.printf("" + disk[j]);
                    count++;
                }
            }
        }

    }

    public int getPositionFile(String name) {
        for (int i = 0; i < filesList.size(); i++) {
            if (filesList.get(i).name.equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public void showPool() {
        int i = 0;
        System.out.println("Pool: \n");
        for (Sector x : pool) {
            System.out.println("Node " + i + " of pool: ");
            System.out.println("    Start: " + x.getStartPoint());
            System.out.println("    End: " + x.getEndPoint());

            i++;
        }
    }

    public void showAllSectorOfEachFile() {
        System.out.println("Sector of each file");
        for (int i = 0; i < filesList.size(); i++) {
            System.out.println("File name " + filesList.get(i).name + ":");
            for (int j = 0; j < filesList.get(i).neighbors.size(); j++) {
                System.out.println("Node " + (j + 1) + ":");
                System.out.println("    Start sector: " + filesList.get(i).neighbors.get(j).getStartPoint());
                System.out.println("    End sector: " + filesList.get(i).neighbors.get(j).getEndPoint());
            }
            System.out.println("");
        }
    }

    public void swapASectorOnFile(File file, int positionLinkedListOfSector) {
        int startPointOfFileSectorAtPosition = file.neighbors.get(positionLinkedListOfSector).getStartPoint();
        int endPointOfFileSectorAtPosition = file.neighbors.get(positionLinkedListOfSector).getEndPoint();
        int startPointOfFirstPoolSector = pool.get(0).getStartPoint();
        int addressDisk = startPointOfFirstPoolSector * 3;

        for (int i = startPointOfFileSectorAtPosition * 3; i < (startPointOfFileSectorAtPosition + 1) * 3; i++) {
            disk[addressDisk] = disk[i];
            addressDisk++;
        }

        if (startPointOfFileSectorAtPosition == endPointOfFileSectorAtPosition) {
            file.neighbors.set(positionLinkedListOfSector, new Sector(startPointOfFirstPoolSector, startPointOfFirstPoolSector));
        } else {
            file.neighbors.get(positionLinkedListOfSector).setStartPoint(startPointOfFileSectorAtPosition + 1);
            file.neighbors.add(positionLinkedListOfSector, new Sector(startPointOfFirstPoolSector, startPointOfFirstPoolSector));
        }

        removeAFirstSectorFromPool();
        addANewSectorToPool(new Sector(startPointOfFileSectorAtPosition, startPointOfFileSectorAtPosition));

    }

    public void removeAFirstSectorFromPool() {
        int startPointOfAPoolSector = pool.get(0).getStartPoint();
        int endPointOfAPoolSector = pool.get(0).getEndPoint();
        if (startPointOfAPoolSector == endPointOfAPoolSector) {
            pool.remove(0);
        } else {
            pool.get(0).setStartPoint(startPointOfAPoolSector + 1);
        }
    }

    public File getFileBySector(Sector sector) {
        for (File file : filesList) {
            for (Sector neighbor : file.neighbors) {
                if (sector.getStartPoint() >= neighbor.getStartPoint() && neighbor.getEndPoint() >= sector.getEndPoint()) {
                    return file;
                }
            }
        }
        return null;
    }

    public int getPositionOfSectorOnFile(File file, Sector sector) {
        int startPoint = sector.getStartPoint();
        for (int i = 0; i < file.neighbors.size(); i++) {
            if (startPoint >= file.neighbors.get(i).getStartPoint() && startPoint <= file.neighbors.get(i).getEndPoint()) {
                return i;
            }
        }
        return -1;
    }

    public void optimizeDisk() {
        int addressOfSectorOptimized = 0;
        for (File file : filesList) {
            int flag = 0;
            for (int i = 0; i < file.neighbors.size(); i++) {
                int startPointOfACurrentSector = file.neighbors.get(i).getStartPoint();
                int endPointOfACurrentSector = file.neighbors.get(i).getEndPoint();
                for (int j = startPointOfACurrentSector; j <= endPointOfACurrentSector; j++) {
                    Sector sector = new Sector(addressOfSectorOptimized, addressOfSectorOptimized);
                    int indexOfDiskElement = addressOfSectorOptimized * 3;
                    for (int k = j * 3; k < (j + 1) * 3; k++) {
                        disk[indexOfDiskElement] = disk[k];
                        indexOfDiskElement++;
                    }
                    removeAFirstSectorFromPool();
                    addANewSectorToPool(new Sector(j, j));

                    if (flag == 0 && startPointOfACurrentSector != endPointOfACurrentSector) {
                        file.neighbors.add(0, new Sector(addressOfSectorOptimized, addressOfSectorOptimized));
                        i++;
                        flag = 1;
                    } else if (j == endPointOfACurrentSector) {
                        file.neighbors.get(0).setEndPoint(addressOfSectorOptimized);
                        file.neighbors.remove(i);
                        i--;
                    } else {
                        file.neighbors.get(0).setEndPoint(addressOfSectorOptimized);
                    }

                    addressOfSectorOptimized++;

                }
            }

        }
    }
}
