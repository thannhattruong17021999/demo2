/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import data.Disk;
import ui.Menu;

/**
 *
 * @author USER
 */
public class DiskManagement {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Menu menu = new Menu("Welcome to sector mangement");
        menu.addNewOption("1. Create a new file");
        menu.addNewOption("2. Delete file");
        menu.addNewOption("3. Show disk");
        menu.addNewOption("4. Show sector of a file");
        menu.addNewOption("5. Show pool");
        menu.addNewOption("6. Optimze");
        menu.addNewOption("7. Show all file and sector of file");
        menu.addNewOption("8. Exit");

        int choice = 1;
        Disk sl = new Disk();
        sl.valueOriginalOfPool();
        do {
            switch (choice) {
                case 1:
                    sl.createFile();
                    break;
                case 2:
                    sl.deleteFile();
                    break;
                case 3:
                    sl.showDisk();
                    break;
                case 4:
                    sl.showSectorOfAFile();
                    break;
                case 5:
                    sl.showPool();
                    break;
                case 6:
                    sl.optimizeDisk();
                    break;
                case 7:
                    sl.showAllSectorOfEachFile();
                    break;

            }
            menu.printMenu();
            choice = menu.getChoice();
        } while (choice != 8);
    }

}
