package ui;

import java.util.ArrayList;
import util.MyToys;

/**
 *
 * @author giao-lang | fb/giao.lang.bis | fb/bmag.vn
 * version 18.03
 */
public class Menu {
    private String menuTitle;
    private ArrayList<String> optionList = new ArrayList();
   
    public Menu(String menuTitle) {
        this.menuTitle = menuTitle;
    }
        
    
    public void addNewOption(String newOption) {
        
        optionList.add(newOption);        
    }
    
    public void printMenu() {
        if (optionList.isEmpty()) {
            System.out.println("There is no item in the menu");
            return;
        }
        System.out.println("\n------------------------------------");
        System.out.println("Welcome to " + menuTitle);
        for (String x : optionList)
            System.out.println(x);  
    }
    
    
    public int getChoice() {
        int maxOption = optionList.size();
        
        String inputMsg = "Choose [1.." + maxOption + "]: ";
        String errorMsg = "You are required to choose the option 1.." + maxOption; 
        return MyToys.getAnInteger(inputMsg, errorMsg, 1, maxOption);
        
    }
}
